

**LOANSOFT DESCRIPTION **
## What it is?
**LOANSOFT** is an online loan management system that allows lending businesses to manage their borrowers, loans, 
repayments, and collections with ease while being affordable at the same time.

##Intuitive & User-friendly

Most functions can be done in one or two clicks. The system has a simple and clear user interface, 
with best-practice workflows taking you through all core processes, and relevant data always at your fingertips.

##Borrowers Management

The system enables the capture of personal information, unique identifiers, next of kin details, business 
information and much more. You can upload scanned photos or documents to a client’s account. You can also 
add your own custom fields.

##Loans & Savings

This system has flexible loan and savings module which you can adjust or customize to suit your microfinance needs. 
Manageable fields include loan term, interest rate, repayment schedule, interest calculation method, charges and many other options.
 Loan approval process follows best microfinance standards

 ##Accounting

You can configure your chart of accounts with GL codes and link them to loan or savings products and many other applicable areas.
 Some journal entries are automatic like loan disbursements while some can be added manually. 
 You can find various accounting reports like trial balance, balance sheet and profit and loss accounts.

 ##Reports

View and export high quality reports. Reports include borrower numbers, loan portfolio, arrears reports, savings reports, 
expected vs actual payments, collection sheets, provisioning and many more

##Staff Management

Manage users with ease. Set permissions for each staff role and control what pages they can see. You can also set payroll for your staff. See audit trail to check what your staff has been doing.